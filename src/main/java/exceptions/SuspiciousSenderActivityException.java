package exceptions;

/**
 * Исключение, выбрасываемое в случае, когда получатель
 * замечен за подозрительной активностью.
 */
public class SuspiciousSenderActivityException extends RuntimeException {
    public SuspiciousSenderActivityException() {
    }

    public SuspiciousSenderActivityException(String message) {
        super(message);
    }
}
