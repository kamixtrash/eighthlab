package exceptions;

/**
 * Исключение, выбрасываемое в случае, когда получатель
 * замечен за подозрительной активностью.
 */
public class SuspiciousRecipientActivityException extends RuntimeException {
    public SuspiciousRecipientActivityException() {
    }

    public SuspiciousRecipientActivityException(String message) {
        super(message);
    }
}
