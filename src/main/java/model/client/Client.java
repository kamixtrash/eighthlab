package model.client;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Objects;

/**
 * Клиент. Может быть как переводящим так и получающим средства лицом.
 */
@Getter
@Setter
@NoArgsConstructor
public abstract class Client {
    private String lastName;
    private String firstName;
    private String patronymic;
    private String cardNumber;
    private Integer accountNumber;
    private Long balance;
    private String pin;

    /**
     * Полный конструктор для клиента.
     *
     * @param lastName      фамилия клиента.
     * @param firstName     имя клиента.
     * @param patronymic    отчество клиента.
     * @param cardNumber    номер карты клиента.
     * @param accountNumber номер счёта клиента.
     * @param balance       общий баланс.
     * @param pin           пин-код.
     */
    public Client(String lastName, String firstName, String patronymic, String cardNumber, Integer accountNumber, Long balance, String pin) {
        this.lastName = lastName;
        this.firstName = firstName;
        this.patronymic = patronymic;
        this.cardNumber = cardNumber;
        this.accountNumber = accountNumber;
        this.balance = balance;
        this.pin = pin;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Client client = (Client) o;
        return Objects.equals(lastName, client.lastName) && Objects.equals(firstName, client.firstName) && Objects.equals(patronymic, client.patronymic) && Objects.equals(cardNumber, client.cardNumber) && Objects.equals(accountNumber, client.accountNumber) && Objects.equals(balance, client.balance) && Objects.equals(pin, client.pin);
    }

    @Override
    public int hashCode() {
        return Objects.hash(lastName, firstName, patronymic, cardNumber, accountNumber, balance, pin);
    }

    @Override
    public String toString() {
        return getLastName() + " " + getFirstName() + " " + getPatronymic();
    }

    public String toStringLower() {
        return toString().toLowerCase();
    }

    public String toStringUpper() {
        return toString().toUpperCase();
    }
}