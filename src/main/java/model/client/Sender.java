package model.client;

public class Sender extends Client {
    /**
     * Полный конструктор для отправителя.
     *
     * @param lastName      фамилия клиента.
     * @param firstName     имя клиента.
     * @param patronymic    отчество клиента.
     * @param cardNumber    номер карты клиента.
     * @param accountNumber номер счёта клиента.
     * @param balance       общий баланс.
     */
    public Sender(String lastName, String firstName, String patronymic, String cardNumber, Integer accountNumber, Long balance, String pin) {
        super(lastName, firstName, patronymic, cardNumber, accountNumber, balance, pin);
    }
}
