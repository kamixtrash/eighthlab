package model.transaction;

import model.client.Client;
import model.currency.Currency;

/**
 * Снятие средств с карты.
 */
public class WithdrawTransaction extends Transaction {
    private static final boolean isWithdraw = true;

    /**
     * @param amount количество средств.
     * @param currency валюта.
     * @param sender пользователь, который желает снять деньги.
     */
    public WithdrawTransaction(Long amount, Currency currency, Client sender) {
        super(amount, currency, null, sender, isWithdraw);
    }
}
