package model.currency;

public enum Currency {
    RUB("Рубль"), EUR("Евро"), USD("Доллар");

    private final String russianCode;

    Currency(String russianCode) {
        this.russianCode = russianCode;
    }

    public String getRussianCode() {
        return russianCode;
    }
}