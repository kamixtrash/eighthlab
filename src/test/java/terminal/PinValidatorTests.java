package terminal;

import exceptions.AccountIsLockedException;
import exceptions.InvalidPinException;
import model.client.Client;
import model.terminal.*;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import static org.mockito.Mockito.when;
import static org.testng.AssertJUnit.assertFalse;

public class PinValidatorTests {

    @Mock
    Client sender;
    @Mock
    FrodMonitor frodMonitor;
    @Mock
    TerminalServer terminalServer;

    Terminal terminal;
    PinValidator pinValidator;
    final String wrongPin = "1233";
    final String rightPin = "1234";

    public PinValidatorTests() {
        MockitoAnnotations.openMocks(this);
    }

    @BeforeMethod
    public void prepareData() {
        pinValidator = new PinValidator();
        terminal = new TerminalImpl(terminalServer, pinValidator, frodMonitor);
    }

    @Test
    public void pinValidatorTest_WWR() {
        System.out.println("Started pin validator test with 2 wrong pins and 1 right.");
        when(sender.getPin()).thenReturn("1234");
        boolean result = true;
        for (int i = 0; i < 2; i++) {
            try {
                pinValidator.validate(wrongPin, sender);
            } catch (InvalidPinException e) {
                result = false;
                System.out.println("Wrong pin detected. Attempt #" + pinValidator.getAttempts());
            }
        }
        assertFalse(result);
        pinValidator.validate(rightPin, sender);
        System.out.println("Finished pin validator test with 2 wrong pins and 1 right.");
    }

    @Test
    public void pinValidatorTest_AllWrongWithAnotherAttempt() {
        System.out.println("Started pin validator test with 3 wrong pins.");
        when(sender.getPin()).thenReturn("1234");
        for (int i = 0; i < 3; i++) {
            try {
                pinValidator.validate(wrongPin, sender);
            } catch (InvalidPinException e) {
                System.out.println("Invalid pin detected. Attempt #" + pinValidator.getAttempts());
            } catch (AccountIsLockedException e) {
                System.out.println("Account lock detected.");
            }
            System.out.println("Finished pin validator test with 3 wrong pins.");
        }
    }

    @Test
    public void pinValidatorTest_Success() {
        System.out.println("Started pin validator successful test.");
        when(sender.getPin()).thenReturn("1234");
        pinValidator.validate(rightPin, sender);
        System.out.println("Finished pin validator successful test.");
    }
}
