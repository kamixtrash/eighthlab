package terminal;

import exceptions.SuspiciousAmountException;
import exceptions.SuspiciousRecipientActivityException;
import model.client.Client;
import model.client.Recipient;
import model.client.Sender;
import model.currency.Currency;
import model.terminal.*;
import model.transaction.Transaction;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.time.LocalDateTime;
import java.util.ArrayList;

import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.when;
import static org.testng.AssertJUnit.assertFalse;

public class FrodMonitorTests {

    @Mock
    TerminalServer terminalServer;
    @Mock
    PinValidator pinValidator;
    @Mock
    Transaction wrongTransaction;
    @Mock
    Client client2;
    @Mock
    Transaction someTransaction;
    @Mock
    Terminal someTerminal;

    Client client1;
    Client client3;
    TerminalImpl terminal;
    FrodMonitor frodMonitor;

    @BeforeMethod
    public void prepareData() {
        MockitoAnnotations.openMocks(this);
        frodMonitor = new FrodMonitor(terminalServer);
        terminal = new TerminalImpl(terminalServer, pinValidator, frodMonitor);
        client1 = new Recipient("Ivanov", "Ivan", "Ivanovich", "RGNJ-1235-8452-RNJF", 345674, 1000L, "1233");
        client3 = new Sender("Scamer", "Scam", "Scam", "RYJN-3463-NJFF-4363", 436356, 10_000_000L, "5555");
    }

    @Test
    public void frodMonitorAmountTest() {
        System.out.println("Starting frod monitor test with suspicious amount.");
        SuspiciousAmountException exception = new SuspiciousAmountException();
        when(terminal.checkTransaction(wrongTransaction, "1234", client3)).thenThrow(exception);
        assertFalse(terminal.checkTransaction(wrongTransaction, "5555", client3));
        System.out.println("Finished frod monitor test with suspicious amount.");
    }

    @Test
    public void frodMonitorSenderTest() {
        System.out.println("Starting frod monitor test with suspicious recipient.");

        FrodMonitor frodMonitor = new FrodMonitor(terminalServer);
        ArrayList<Transaction> transactions = new ArrayList<>();
        for (int i = 0; i < 10; i++) {
            transactions.add(someTransaction);
        }
        doNothing().when(pinValidator).validate("5555", client3);
        when(terminalServer.getTransactions()).thenReturn(transactions);
        when(someTransaction.getSender()).thenReturn(client3);
        when(someTransaction.getRecipient()).thenReturn(client1);
        when(someTransaction.getAmount()).thenReturn(150_000L);
        when(someTransaction.getTransactionDate()).thenReturn(LocalDateTime.now());

        try {
            frodMonitor.checkTransaction(transactions.get(transactions.size() - 1));
        } catch (SuspiciousRecipientActivityException e) {
            System.out.println("Detected suspicious recipient activity.");
        }

        System.out.println("Finished frod monitor test with suspicious recipient.");
    }
}
