package transactions;

import exceptions.InsufficientFundsException;
import exceptions.WrongAmountException;
import model.client.Client;
import model.terminal.FrodMonitor;
import model.terminal.PinValidator;
import model.terminal.TerminalImpl;
import model.terminal.TerminalServer;
import model.transaction.Transaction;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import static org.mockito.Mockito.when;
import static org.testng.AssertJUnit.assertFalse;

public class TransactionTests {

    @Mock
    Transaction wrongTransaction;
    @Mock
    Client client2;
    @Mock
    PinValidator pinValidator;
    @Mock
    TerminalServer terminalServer;
    @Mock
    FrodMonitor frodMonitor;

    TerminalImpl terminal;

    public TransactionTests() {
        MockitoAnnotations.openMocks(this);
    }

    @BeforeMethod
    public void prepareData() {
        terminal = new TerminalImpl(terminalServer, pinValidator, frodMonitor);
    }

    @Test
    public void transactionInsFundsExceptionTest_ShouldCatchException() {
        System.out.println("Starting transaction test with not enough money.");
        InsufficientFundsException exception = new InsufficientFundsException();
        when(terminal.commitTransaction(wrongTransaction, "1234", client2)).thenThrow(exception);
        assertFalse(terminal.commitTransaction(wrongTransaction, "1234", client2));
        System.out.println("Finished transaction test with not enough money.");
    }

    @Test
    public void transactionWrongAmountTest_ShouldCatchException() {
        System.out.println("Starting transaction test with wrong amount of money.");
        WrongAmountException exception =  new WrongAmountException();
        when(terminal.checkTransaction(wrongTransaction, "1234", client2)).thenThrow(exception);
        assertFalse(terminal.checkTransaction(wrongTransaction, "1234", client2));
        System.out.println("Finished transaction test with wrong amount of money.");
    }
}
